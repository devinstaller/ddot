import { DokzProvider,  ColorModeSwitch } from 'dokz'
import React, { Fragment } from 'react'
import Head from 'next/head'

import {
    Box,
    PseudoBox,
} from '@chakra-ui/core'

import { DiGithubBadge } from 'react-icons/di'
import { RiGitlabLine } from "react-icons/ri"

export const IconLink = ({ logo = "",  ...rest }: any) => (
    <PseudoBox
        as='a'
        href="#"
        rel='noopener noreferrer'
        target='_blank'
        aria-label="test"
        outline='0'
        transition='all 0.2s'
        borderRadius='md'
        _focus={{
            boxShadow: 'outline',
        }}
        {...rest}
    >
        <Box as={logo} size='8' color='current' />
    </PseudoBox>
)

export default function App(props: { Component: any; pageProps: any }) {
    const { Component, pageProps } = props
    return (
        <Fragment>
            <Head>
                <link
                    href='https://fonts.googleapis.com/css?family=Fira+Code'
                    rel='stylesheet'
                    key='google-font-Fira'
                />
            </Head>
            <DokzProvider
                headerItems={[
                    <IconLink
                        key='0'
                        logo={RiGitlabLine}
                        aria-label="Go to gitlab page"
                        href='https://gitlab.com/devinstaller/ddot'
                    />,
                    <IconLink
                        key='1'
                        logo={DiGithubBadge}
                        aria-label="Go to github page"
                        href='https://github.com/devinstaller/ddot'
                    />,
                    <ColorModeSwitch key='2' />,
                ]}
                sidebarOrdering={{
                    'index.mdx': true,
                }}
            >
                <Component {...pageProps} />
            </DokzProvider>
        </Fragment>
    )
}

